Gem::Specification.new do |s|
  s.name        = 'wialon'
  s.version     = '2.0.0'
  s.date        = Time.now.strftime("%Y-%m-%d")
  s.summary     = "Wialon Remote API for Ruby"
  s.description = "Wialon Remote API for Ruby, will support Wialon Hosting, Wialon Local and Wialon Pro"
  s.authors     = ["Golden M Software", "Kenny Mochizuki"]
  s.email       = 'software@goldenmcorp.com'
  s.files       = ["lib/wialon.rb"]
  s.homepage    = 'https://gitlab.com/goldenm-software/open-source-libraries/wialon-ruby'
  s.license     = 'MIT'
end